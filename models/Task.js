const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    title: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    status: {
        type: String,
        enum: ['new', "in_progress", "complete"],
        default: 'new'
    }
});

TaskSchema.plugin(idValidator, {
    message: 'Such {PATH} doesn`t exist'
});

TaskSchema.set("toJSON", {
    transform: (doc, ret, options) => {
        delete ret.user;
        return ret;
    }
});



const Task = mongoose.model('Task', TaskSchema);
module.exports = Task;