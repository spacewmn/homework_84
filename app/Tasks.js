const router = require("express").Router();
const Tasks = require("../models/Task");
const auth = require('../middleware/auth');

router.get("/", auth, async (req, res) => {
    try {
        const tasks = await Tasks.find();
        res.send(tasks);
    } catch (e) {
        res.sendStatus(500);
    }
});

    router.post("/", auth, async (req, res) => {
        console.log(req.body);
        let task = new Tasks(req.body);
        try {
            await task.save();
            res.send(task);
        } catch(e) {
            console.log(e);
            res.status(404).send(e);
        }
    });

router.put("/:id", auth, async (req, res) => {
    let taskId = await Tasks.findById({_id: req.params.id});
    if (req.body.user === req.user._id.toString()) {

        try {
            await taskId.set(req.body);
            let newTask = await taskId.save();
            res.send(newTask);
        } catch(e) {
            console.log(e);
            res.status(500).send(e);
        }
    } else {
        res.send("you")
    }
});

router.delete("/:id", auth, async (req, res) => {
    const taskId = await Tasks.findById(req.params.id);

    try {
        await taskId.delete();
        res.send("This task was successfully deleted");
    } catch(e) {
        console.log(e);
        res.status(500).send(e);
    }
});

module.exports = router;