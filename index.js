const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const port = 8000;
const users = require("./app/Users");
const tasks = require("./app/Tasks");

app.use(cors());
app.use(express.json());

const run = async () => {
    await mongoose.connect("mongodb://localhost/lesson84", {useNewUrlParser: true});

    app.use("/users", users);
    app.use("/tasks", tasks);

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);